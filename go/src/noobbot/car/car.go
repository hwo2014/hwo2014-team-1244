package car

import (
    "fmt"
    "math"
    "encoding/json"
    "io/ioutil"
    "os"
)

type CarDimension struct {
    length, width, guideFlagPosition float64
}

type Car struct {
    name, color string
    dimension CarDimension
    angle, lastAngle float64
    piecePosition, lastPosition PiecePosition
    nextBendPiece float64
    throttle, lastThrottle float64
    wantToSwitchLane string
    nextSwitch, nextNextSwitch int
    maxValues map[KeyType]*ValueType
    isInitialized bool
    race *Race
}

type ValueType struct {
    V, VMax float64
    MaxAngle float64
    VMaxLock bool
}

type KeyType struct {
    Angle, Radius float64
}

type KeyValue struct {
    Key KeyType
    Value ValueType
}

type Values struct {
    KeyValues []KeyValue
}

func NewCar(name string, color string, dimension CarDimension, race *Race) *Car {
    carl := new(Car)
    
    carl.name = name
    carl.color = color
    carl.dimension = dimension
    carl.nextSwitch = -1
    carl.nextNextSwitch = -1
    carl.isInitialized = false
    carl.maxValues = make(map[KeyType]*ValueType)
    carl.race = race
    
    track := carl.race.track
    MinSpeed = 5.8
    
    // initialize max values
    if ! carl.isInitialized {
        carl.isInitialized = true
        for i, p := range track.pieces {
            for _, lane := range track.lanes {
                angle := 0.0
                radius := p.Radius(lane)
                
                for j:= 0; ! track.pieces[(i + j) % len(track.pieces)].IsStraight(); j++ {
                    piece := track.pieces[(i + j) % len(track.pieces)]
                    
                    angle += math.Abs(piece.Angle())
                    radius = math.Min(radius, piece.Radius(lane))
                }
                myKey := KeyType{Angle:angle, Radius:radius}
                
                if ! p.IsStraight() {
                    valueType := new(ValueType)
                    valueType.V = MinSpeed
                    valueType.VMax = valueType.V
                    carl.maxValues[myKey] = valueType
                }
            }
        }
    }
    
    carl.ReadVelocityFile("src/velocity.json")
    return carl
}

func (this *Car) ReadVelocityFile(filename string) (err error) {
    file, err := ioutil.ReadFile(filename)
    if err != nil {
        fmt.Printf("File error: %v\n", err)
        os.Exit(1)
    }
 
    var keyValues Values
    err = json.Unmarshal(file, &keyValues)
    if err != nil {
        fmt.Println("Error:", err)
        os.Exit(1)
    }
    
    for _, val := range keyValues.KeyValues {
        valueType := new(ValueType)
        valueType.V = val.Value.V
        valueType.VMax = val.Value.VMax
        valueType.VMaxLock = val.Value.VMaxLock
        valueType.MaxAngle = val.Value.MaxAngle
        this.maxValues[val.Key] = valueType
    }
    
    return
}

func (this *Car) WriteVelocityFile(filename string) (err error) {
    var keyValues Values
    for key, value := range this.maxValues {
        keyValues.KeyValues = append(keyValues.KeyValues, KeyValue{Key:key, Value:*value})
    }
    
    byte, err := json.Marshal(keyValues)
    if err != nil {
        fmt.Println("Error:", err)
    } else {
        ioutil.WriteFile(filename, byte, 0666)
    }
    
    return
}

func (this *Car) Update(data interface{}) (err error) {
    carJson := data.(map[string]interface{})
    
    this.lastAngle = this.angle
    this.angle = carJson["angle"].(float64)
    
    piecePosition := carJson["piecePosition"].(map[string]interface{})
    this.lastPosition = this.piecePosition
    this.piecePosition.pieceIndex = int(piecePosition["pieceIndex"].(float64))
    this.piecePosition.inPieceDistance = piecePosition["inPieceDistance"].(float64)
    this.piecePosition.lap = int(piecePosition["lap"].(float64))
    
    lane := piecePosition["lane"].(map[string]interface{})
    this.piecePosition.startLaneIndex = int(lane["startLaneIndex"].(float64))
    this.piecePosition.endLaneIndex = int(lane["endLaneIndex"].(float64))
    
    return
}

func (this *Car) CalculateThrottle() (throttle float64, switchLane string, turbo bool, err error) {
    //===========================
    //            Vars
    //===========================
    track := this.race.track
    pieceIndex := this.piecePosition.pieceIndex
    // set current piece max values    
    currentPiece := track.pieces[pieceIndex]
    currentLane := this.Lane(&track)
    criticalAngle := 56.0
    step := 0.01
    var angle float64
    var radius float64
    var maxThrottle, minThrottle float64
    maxThrottle = 1.0
    minThrottle = 0.0
    //speedDifference := 0.5
    requiredStraightsForTurbo := 4
    
    //===========================
    //     enemy car switch!
    //===========================
    switched := false
    for _, c := range this.race.cars {
        if c.color != this.color {
            if c.piecePosition.pieceIndex == pieceIndex || c.piecePosition.pieceIndex == (pieceIndex + 1) % len(track.pieces) {
                if c.piecePosition.startLaneIndex == this.piecePosition.startLaneIndex {
                    if this.Velocity() > c.Velocity() {
                        if this.Lane(&track).index < len(track.lanes) {
                            this.wantToSwitchLane = "Right"
                        } else {
                            this.wantToSwitchLane = "Left"
                        }
                        switched = true
                    }
                }
            }
        }
    }
    
    //===========================
    //       switch Lane
    //===========================    
    // switch to inner lane
    if this.nextSwitch == -1 {
        this.nextSwitch = track.NextSwitch(pieceIndex)
        this.nextNextSwitch = track.NextSwitch(this.nextSwitch)
        curveAngle := track.CurveAngle(this.nextSwitch, this.nextNextSwitch)
        
        if ! switched {
            if curveAngle > 0.0 {
                if this.Lane(&track).index < len(track.lanes) {
                    this.wantToSwitchLane = "Right"
                }
            } else if curveAngle < 0.0 {
                if this.Lane(&track).index > 0 { //< len(track.lanes) {
                    this.wantToSwitchLane = "Left"
                }
            }
        }
    }

    if pieceIndex == ((this.nextSwitch - 1) % len(track.pieces)) && this.piecePosition.inPieceDistance + this.Velocity() * 2 > currentPiece.Length(currentLane) {
        if (this.wantToSwitchLane != "") {
            switchLane = this.wantToSwitchLane
            this.wantToSwitchLane = ""
        }
        
        //this.wantToSwitchLane = ""
        this.nextSwitch = -1
        switched = false
    }
    if pieceIndex >= this.nextSwitch {
        //this.wantToSwitchLane = ""
        this.nextSwitch = -1
        switched = false
    }

    //===========================
    //     check max Angle
    //===========================
    angle = 0.0
    radius = currentPiece.Radius(currentLane)
    for j:= 0; ! track.Piece(pieceIndex + j).IsStraight(); j++ {
        piece := track.Piece(pieceIndex + j)
        
        angle += math.Abs(piece.Angle())
        radius = math.Min(radius, piece.Radius(this.Lane(&track)))
    }
    myKey := KeyType{Angle:angle, Radius:radius}
    
    fmt.Println("curveAngle", angle, ", curveRadius", currentPiece.Radius(currentLane), ", carAngle:", this.Angle(), "current V", this.Velocity(), "current piece", pieceIndex)
    pieceVelocity, ok := this.maxValues[myKey]
    if ok {
        pieceVelocity.MaxAngle = math.Max(this.Angle(), pieceVelocity.MaxAngle)
        if ! this.race.TurboActivated() {
            if this.Angle() <= criticalAngle {
                if ! pieceVelocity.VMaxLock {
                    // dynamic step
                    if this.Angle() < 20 {
                        step = 0.1
                    } else if this.Angle() < 40 {
                        step = 0.05
                    } else {
                        step = 0.01
                    }
                    pieceVelocity.VMax = math.Max(pieceVelocity.V, this.Velocity() + math.Max(1 / math.Max(this.Velocity() * this.Velocity(), 1.0), step))
                    pieceVelocity.V = pieceVelocity.VMax
                }
            } else {
                pieceVelocity.VMaxLock = true
                pieceVelocity.V = pieceVelocity.VMax - (this.Angle() - criticalAngle) * 0.1
            }
        }
        /*for key, value := range this.maxValues {
            if key.Angle == myKey.Angle {
                if key.Radius > myKey.Radius {
                    value.MaxAngle = pieceVelocity.MaxAngle
                    value.V = pieceVelocity.V + speedDifference
                    value.VMax = pieceVelocity.VMax + speedDifference
                    value.VMaxLock = pieceVelocity.VMaxLock
                } else if key.Radius < myKey.Radius {
                    value.MaxAngle = pieceVelocity.MaxAngle
                    value.V = pieceVelocity.V - speedDifference
                    value.VMax = pieceVelocity.VMax - speedDifference
                    value.VMaxLock = pieceVelocity.VMaxLock
                }
            }
        }*/
    }
    
    fmt.Println("--------------------")
    for idx, value := range this.maxValues {
        fmt.Println("Key", idx, "v:", value.V, "vMax:", value.VMax, "vMaxLock:", value.VMaxLock, ", maxAngle", value.MaxAngle)
    }
    fmt.Println("--------------------")
    
    //===========================
    //         Turbo
    //===========================
    allStraight := true
    for i := 0; i < requiredStraightsForTurbo; i++ {
        if ! track.pieces[(i + pieceIndex) % len(track.pieces)].IsStraight() {
            allStraight = false
            break
        }
    }
    if allStraight { //&& GameTick > 890 { // && this.piecePosition.lap == 2 {
        turbo = true
        //this.throttle = 0.0
    }
    
    //===========================
    //         Throttle
    //===========================
    if track.GetNextSpeed(this) {
        this.throttle = 0.0
    } else {
        /*if this.Angle() >= criticalAngle - 10 {
            this.throttle = 0.0
        } else*/ {
            this.throttle = 1.0
        }
    }
    
    if this.throttle < minThrottle {
        this.throttle = minThrottle
    }
    if this.throttle > maxThrottle {
        this.throttle = maxThrottle
    }
    
    throttle = this.throttle
    
    return
}

func (this *Car) Throttle() (throttle float64) {    
    throttle = this.throttle
    return
}

func (this *Car) Velocity() float64 {
    // todo calculate velocity for bended pieces properly
	velocity := this.piecePosition.inPieceDistance - this.lastPosition.inPieceDistance
    
    if this.piecePosition.pieceIndex != this.lastPosition.pieceIndex {
        velocity += this.race.track.Piece(this.lastPosition.pieceIndex).Length(this.Lane(&this.race.track))
    }
    return velocity
}

func (this *Car) Lane(track *Track) Lane {
    laneIdx := this.piecePosition.startLaneIndex
    
    return track.lanes[laneIdx]
}

func (this *Car) Angle() (angle float64) {
    angle = this.angle
    if angle < 0 {
        angle *= -1
    }
    return
}