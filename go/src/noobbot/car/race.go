package car

var GameTick int
var GameID string
var MinSpeed float64

type Turbo struct {
    time int
    timeInMSec float64
    factor float64
}

type Race struct {
    track Track
    cars map[string]*Car
    carl *Car
    raceSession RaceSession
    turbo Turbo
    isTurboAvailable bool
    isTurboActivated bool
}

func (this *Race) Carl() *Car {
    return this.carl
}

func (this *Race) UseTurbo() (err error) {
    this.isTurboAvailable = false
    this.isTurboActivated = true
    
    return
}

func (this *Race) TurboActivated() (active bool) {
    return this.isTurboActivated
}

func (this *Race) SetTurbo(data interface{}) (err error) {
    turboJson := data.(map[string]interface{})
    this.turbo.timeInMSec = turboJson["turboDurationMilliseconds"].(float64)
    this.turbo.time = int(turboJson["turboDurationTicks"].(float64))
    this.turbo.factor = turboJson["turboFactor"].(float64)
    
    this.isTurboAvailable = this.turbo.time > 0
    
    return
}

func (this *Race) TurboAvailable() (bool) {
    return this.isTurboAvailable
}

func (this *Race) Create(data interface{}, color string) (err error) {
    // Contains the Race infos
	raceMap := data.(map[string]interface{})["race"].(map[string]interface{})
	
	// Track
    this.track.Create(raceMap["track"])
	
	
	// Cars for the Race
	carsMap := raceMap["cars"].([]interface{})
    this.cars = make(map[string]*Car)
	for _, v := range carsMap {
        carJson := v.(map[string]interface{})
        
		//var carl Car
		id := carJson["id"].(map[string]interface{})
		dimension := carJson["dimensions"].(map[string]interface{})
        
		this.cars[id["color"].(string)] = NewCar(id["name"].(string), id["color"].(string), CarDimension{length:dimension["length"].(float64), width:dimension["width"].(float64), guideFlagPosition:dimension["guideFlagPosition"].(float64)}, this)
	}
    
    this.carl = this.cars[color]
	
	// Race Session
    this.raceSession.Create(raceMap["raceSession"])
	
    return
}

func (this *Race) Update(data interface{}) (throttle float64, switchLane string, turbo bool) {
    carsMap := data.([]interface{})
    for _, v := range carsMap {
        carJson := v.(map[string]interface{})
        
        color := carJson["id"].(map[string]interface{})["color"].(string)
        this.cars[color].Update(v)
    }
    
    if this.isTurboActivated {
        this.turbo.time--
        if this.turbo.time <= 1 {
            this.isTurboActivated = false
        }
    }
    
    throttle, switchLane, turbo, _ = this.carl.CalculateThrottle()
    
    return
}