package car

import (
    "math"
)

type StartingPoint struct {
    x, y, angle float64
}

type Track struct {
    id, name string
    pieces []Piece
    lanes []Lane
    startingPoint StartingPoint
}

func (this *Track) Create(data interface{}) (err error) {
    trackMap := data.(map[string]interface{})
    
    this.id = trackMap["id"].(string)
    this.name = trackMap["name"].(string)
	
	// Pieces in Track
	piecesMap := trackMap["pieces"].([]interface{})
	for _, v := range piecesMap {
        pieceJson := v.(map[string]interface{})
		if _, ok := pieceJson["length"]; ok {
            isSwitch := pieceJson["switch"]
            if isSwitch == nil {
                isSwitch = false
            }
			piece := StraightPiece{length:pieceJson["length"].(float64), isSwitch:isSwitch.(bool)}
			this.pieces = append(this.pieces, piece)
		} else {
            isSwitch := pieceJson["switch"]
            if isSwitch == nil {
                isSwitch = false
            }
			piece := BendPiece{radius:pieceJson["radius"].(float64), angle:pieceJson["angle"].(float64), isSwitch:isSwitch.(bool)}
			this.pieces = append(this.pieces, piece)
		}
	}
	
	// Lanes in Track
	lanesMap := trackMap["lanes"].([]interface{})
	for _, v := range lanesMap {
        laneJson := v.(map[string]interface{})
		
		lane := Lane{distanceFromCenter:laneJson["distanceFromCenter"].(float64), index:int(laneJson["index"].(float64))}
		this.lanes = append(this.lanes, lane)
	}
	
	// StartingPoint in Track
	startingPoint := trackMap["startingPoint"].(map[string]interface{})
	position := startingPoint["position"].(map[string]interface{})
	this.startingPoint = StartingPoint{x:position["x"].(float64), y:position["y"].(float64), angle:startingPoint["angle"].(float64)}
    
    return
}

func (this *Track) NextPiece(currentPieceIdx int) (nextPiece Piece) {
	nextPiece = (this.pieces[(currentPieceIdx + 1) % len(this.pieces)])
	return
}

func (this *Track) Piece(currentPieceIdx int) (Piece) {
	return this.pieces[(currentPieceIdx) % len(this.pieces)]
}

func (this *Track) NextSwitch(currentPieceIdx int) (nextSwitch int) {
    for i := 1; i < len(this.pieces); i++ {
        if this.pieces[(i + currentPieceIdx) % len(this.pieces)].IsSwitch() {
            return (i + currentPieceIdx) % len(this.pieces)
        }
    }
    return -1
}

func (this *Track) CurveAngle(piece1Idx int, piece2Idx int) (angle float64) {
    for i := piece1Idx + 1; i <= piece2Idx - 1; i++ {
        angle += this.pieces[i % len(this.pieces)].Angle()
    }
    return
}

func (this *Track) GetNextSpeed(car *Car) (brake bool) {
    currentPieceIndex := car.piecePosition.pieceIndex
    inPieceDistance := car.piecePosition.inPieceDistance
    maxValues := car.maxValues
    currentLane := car.Lane(this)
    
    factor := 0.008
    minSpeed := MinSpeed
    
    velocity := car.Velocity()

    var numPieces int
    {
        overSpeed := math.Max(velocity - minSpeed, 0.0)
        avgVelocity := velocity- overSpeed / 1.5
        neededTicks := math.Ceil(overSpeed / (factor * velocity))
        distance := math.Ceil(neededTicks * avgVelocity)
        absoluteDistance := distance + inPieceDistance
        
        k := currentPieceIndex
        for ; absoluteDistance >= 0; k++ {
            absoluteDistance -= this.Piece(k).Length(currentLane)
        }
        numPieces = int(math.Min(float64(k), float64(len(this.pieces))))
    }
    curves := make(map[int]*ValueType)
    
    var myKey KeyType
    for i := 0; i < numPieces; i++ {
        j := 0
        var angle, radius float64
        radius = this.Piece(currentPieceIndex + i + j).Radius(currentLane)
        for j = 0; ! this.Piece(currentPieceIndex + i + j).IsStraight() && j < len(this.pieces); j++ {
            piece := this.Piece(currentPieceIndex + i + j)
            
            angle += math.Abs(piece.Angle())
            radius = math.Min(radius, piece.Radius(currentLane))
        }
        if j > 0 {
            myKey = KeyType{Angle:angle, Radius:radius}
            curves[i] = maxValues[myKey]
        }
    }
    
    for index, value := range curves {
        overSpeed := car.Velocity() - value.V
        avgVelocity := car.Velocity() - overSpeed / 1.5
        neededTicks := math.Ceil(overSpeed / (factor * velocity))
        distance := math.Ceil(neededTicks * avgVelocity)
        absoluteDistance := distance + inPieceDistance
        
        for k := currentPieceIndex; absoluteDistance >= 0; k++ {
            absoluteDistance -= this.Piece(k).Length(currentLane)
            if k == index + currentPieceIndex {
                if absoluteDistance < 0.0 && absoluteDistance + this.Piece(k).Length(currentLane) >= this.Piece(k).Length(currentLane) / 2.0 {
                    // speed on half curve
                } else {
                    brake = true
                    return
                }
            }
        }
    }
    
    return
}