package car

import "math"

type Piece interface {
    IsStraight() bool
    IsSwitch() bool
    Length(lane Lane) float64
    Angle() float64
    Radius(lane Lane) float64
}

type PiecePosition struct {
    pieceIndex, lap int
    inPieceDistance float64
    startLaneIndex, endLaneIndex int
}

type StraightPiece struct {
    length float64
    isSwitch bool
}

func (this StraightPiece) IsStraight() (piece bool) {
    piece = true
    return
}

func (this StraightPiece) IsSwitch() (piece bool) {
    return this.isSwitch
}

func (this StraightPiece) Length(lane Lane) (length float64) {
    length = this.length
    return
}

func (this StraightPiece) Angle() (angle float64) {
    return 0.0
}

func (this StraightPiece) Radius(lane Lane) (radius float64) {
    return 0.0
}

type BendPiece struct {
    radius, angle float64
    isSwitch bool
}

func (this BendPiece) IsStraight() (piece bool) {
    piece = false
    return
}

func (this BendPiece) IsSwitch() (piece bool) {
    return this.isSwitch
}

func (this BendPiece) Length(lane Lane) (length float64) {
    length = ((2 * this.Radius(lane)) * math.Pi) / 360.0 * math.Abs(this.angle)
    
    return
}

func (this BendPiece) Angle() (angle float64) {
    return this.angle
}

func (this BendPiece) Radius(lane Lane) (radius float64) {
    if this.angle > 0 {
        radius = this.radius - lane.distanceFromCenter
    } else {
        radius = this.radius + lane.distanceFromCenter
    }
    
    return radius
}