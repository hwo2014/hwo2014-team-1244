package car

type RaceSession struct {
    laps int
    maxLapTimeMs int
    quickRace bool
}

func (this *RaceSession) Create(data interface{}) (err error) {
	raceSessionMap := data.(map[string]interface{})
    
    laps, ok := raceSessionMap["laps"]
    if ok {
        this.laps = int(laps.(float64))
    }
    
    durationMs, ok := raceSessionMap["durationMs"]
    if ok {
        this.maxLapTimeMs = int(durationMs.(float64))
    }
    
    maxLapTimeMs, ok := raceSessionMap["maxLapTimeMs"]
    if ok {
        this.maxLapTimeMs = int(maxLapTimeMs.(float64))
    }
    
    quickRace, ok := raceSessionMap["quickRace"]
    if ok {
        this.quickRace = quickRace.(bool)
    }
    
    return
}