package main

import (
    "bufio"
    "encoding/json"
    "errors"
    "fmt"
    "log"
    "net"
    "os"
    "strconv"
    
    "noobbot/car"
)

var race car.Race
var color string

func connect(host string, port int) (conn net.Conn, err error) {
    conn, err = net.Dial("tcp", fmt.Sprintf("%s:%d", host, port))
    return
}

func read_msg(reader *bufio.Reader) (msg interface{}, err error) {
    var line string
    line, err = reader.ReadString('\n')
    if err != nil {
        return
    }
	
	log_debug(line);
	
    err = json.Unmarshal([]byte(line), &msg)
    if err != nil {
        return
    }
    return
}

func write_msg(writer *bufio.Writer, msgtype string, data interface{}) (err error) {
    m := make(map[string]interface{})
    m["msgType"] = msgtype
    m["data"] = data
    var payload []byte
    payload, err = json.Marshal(m)
    _, err = writer.Write([]byte(payload))
    if err != nil {
        return
    }
    _, err = writer.WriteString("\n")
    if err != nil {
        return
    }
    writer.Flush()
    return
}

func send_join(writer *bufio.Writer, name string, key string) (err error) {
    data := make(map[string]string)
    data["name"] = name
    data["key"] = key
    err = write_msg(writer, "join", data)
    return
}

func send_joinCustom(writer *bufio.Writer, track string, pswd string, carCount int, name string, key string) (err error) {
	data := make(map[string]interface{})
	
	botId := make(map[string]string)
	botId["name"] = name
	botId["key"] = key
	
	data["botId"] = botId
	data["trackName"] = track
	
	if pswd == "" {
		data["carCount"] = 1
	} else {
		data["password"] = pswd
		data["carCount"] = carCount
	}
	
	err = write_msg(writer, "joinRace", data)
	return
}

func send_ping(writer *bufio.Writer) (err error) {
    err = write_msg(writer, "ping", make(map[string]string))
    return
}

func send_throttle(writer *bufio.Writer, throttle float64) (err error) {
    err = write_msg(writer, "throttle", throttle)
    return
}

func send_switch(writer *bufio.Writer, switchLane string) (err error) {
	err = write_msg(writer, "switchLane", switchLane)
	return
}

func send_turbo(writer *bufio.Writer, msg string) (err error) {
	err = write_msg(writer, "turbo", msg)
	return
}

func send_create(writer *bufio.Writer, track string, pswd string, carCount int, name string, key string) (err error) {
	data := make(map[string]interface{})
	
	botId := make(map[string]string)
	botId["name"] = name
	botId["key"] = key
	
	data["botId"] = botId
	data["trackName"] = track
	data["password"] = pswd
	data["carCount"] = carCount
	
	err = write_msg(writer, "createRace", data)
	return
}

func createRace(data interface{}, color string) (err error) {
	race.Create(data, color)
	log.Printf(fmt.Sprintf("race:%+v\n", race))
	
	return
}

func dispatch_msg(writer *bufio.Writer, msgtype string, data interface{}) (err error) {
    switch msgtype {
    case "joinRace":
		fallthrough
	case "join":
        log.Printf("Joined")
        send_ping(writer)
	case "yourCar":
		msg := data.(map[string]interface{})
		color = msg["color"].(string)
		log.Printf(fmt.Sprintf("Your car arrived it's %s", color))
		send_ping(writer)
	case "gameInit":
		// Init Objects with Race-Data
		createRace(data, color)
		//race.Carl().WriteVelocityFile("src/velocity.json")
		//os.Exit(0)
		
		send_ping(writer)
    case "gameStart":
		// The Game starts now, no new Data
        log.Printf("Game started")
        //send_ping(writer)
		send_throttle(writer, 1.0)
    case "carPositions":
		throttle, switchLane, turbo := race.Update(data)
		if switchLane == "" {
			if turbo && race.TurboAvailable() {
				send_turbo(writer, "TO INFINITY AND BEYOND")
				race.UseTurbo()
			} else {
				send_throttle(writer, throttle)
			}
		} else {
			send_switch(writer, switchLane)
		}
	case "turboAvailable":
		race.SetTurbo(data)
	case "turboStart":
		log.Printf("Turbo started")
		send_ping(writer)
	case "turboEnd":
		log.Printf("Turbo ended")
		send_ping(writer)
    case "crash":
        log.Printf("Someone crashed")
        send_ping(writer)
    case "gameEnd":
        log.Printf("Game ended")
		
		race.Carl().WriteVelocityFile("src/velocity.json")
		send_ping(writer)
		//os.Exit(0) // TODO weg
	case "tournamentEnd":
		log.Printf("Tournament ended")
		os.Exit(0)
    case "error":
        log.Printf(fmt.Sprintf("Got error: %v", data))
        send_ping(writer)
    default:
        log.Printf("Got msg type: %s", msgtype)
        send_ping(writer)
		// dnf, finish, spawn, lapFinished
    }
    return
}

func parse_and_dispatch_input(writer *bufio.Writer, input interface{}) (err error) {	
    switch t := input.(type) {
    default:
        err = errors.New(fmt.Sprintf("Invalid message type: %T", t))
        return
    case map[string]interface{}:
        //var msg map[string]interface{}			
        //var ok bool
        msg, ok := input.(map[string]interface{})
        if !ok {
            err = errors.New(fmt.Sprintf("Invalid message type: %v", msg))
            return
        }
		
		if _, ok := msg["gameTick"]; ok {
			car.GameTick = int(msg["gameTick"].(float64))
		}
		
        switch msg["data"].(type) {
        default:
            err = dispatch_msg(writer, msg["msgType"].(string), nil)
            if err != nil {
                return
            }
        case interface{}:
			if msg["msgType"].(string) == "gameInit" {
				if _, ok := msg["gameID"]; ok {
					car.GameID = msg["gameID"].(string)
				}
			}
		
            err = dispatch_msg(writer, msg["msgType"].(string), msg["data"].(interface{}))
            if err != nil {
                return
            }
        }
    }
    return
}

func bot_loop(conn net.Conn, name string, key string, trackName string, password string, create bool) (err error) {
    reader := bufio.NewReader(conn)
    writer := bufio.NewWriter(conn)
	if trackName == "" {
		send_join(writer, name, key)
	} else {
		if (create) {
			send_create(writer, trackName, password, 2, name, key)
		} else {
			send_joinCustom(writer, trackName, password, 2, name, key)
		}
	}
    for {
        input, err := read_msg(reader)
        if err != nil {
            log_and_exit(err)
        }
        err = parse_and_dispatch_input(writer, input)
        if err != nil {
            log_and_exit(err)
        }
    }
}

func parse_args() (host string, port int, name string, key string, err error, trackName string, password string, create bool) {
    args := os.Args[1:]
    if len(args) != 4 {
		if len(args) >= 5 && len(args) <= 7 {
			trackName = args[4]
			if len(args) > 5 {
				password = args[5]
				create, err = strconv.ParseBool(args[6])
				if err != nil {
					create = false
				}
			}
		} else {
			return "", 0, "", "", errors.New("Usage: ./run host port botname botkey"), "", "", false
		}
    }
    host = args[0]
    port, err = strconv.Atoi(args[1])
    if err != nil {
        return "", 0, "", "", errors.New(fmt.Sprintf("Could not parse port value to integer: %v\n", args[1])), "", "", false
    }
    name = args[2]
    key = args[3]

    return
}

func log_debug(msg string) {
    log.Print(msg)
}

func log_and_exit(err error) {
    log.Fatal(err)
    os.Exit(1)
}

func main() {
	host, port, name, key, err, trackName, password, create := parse_args()
	
    //host, port, name, key, err := parse_args()

    if err != nil {
        log_and_exit(err)
    }
	
	logfile, err := os.OpenFile("log.txt", os.O_RDWR | os.O_CREATE, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	defer logfile.Close()
	//log.SetOutput(logfile)

    fmt.Println("Connecting with parameters:")
    fmt.Printf("host=%v, port=%v, bot name=%v, key=%v", host, port, name, key)
	if trackName != "" {
		fmt.Printf(",track=%v, password='%v', is server:%v\n", trackName, password, create)
	} else {
		fmt.Printf("\n")
	}

    conn, err := connect(host, port)

    if err != nil {
        log_and_exit(err)
    }

    defer conn.Close()

    err = bot_loop(conn, name, key, trackName, password, create)
}
